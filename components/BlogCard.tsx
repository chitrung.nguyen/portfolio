import React, { memo } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import dynamic from 'next/dynamic'
import { motion } from 'framer-motion'
import { ArrowLongRightIcon, CalendarDaysIcon } from '@heroicons/react/24/solid'

import { IBlog } from '@/types'
import { urlFor } from '@/utils/sanity/sanity'
import { blogVariants } from '@utils/famer-motion'
import { formatDate, randomColor, shimmer, toBase64 } from '@/utils'

const Tag = dynamic(() => import('@components/@cores').then(module => module.Tag), { ssr: false })

interface Props {
  blog: IBlog
}

const BlogCard: React.FC<Props> = ({ blog }) => {
  const { title, thumbnailImg, publishedAt, author: { name: authorName = '', image: authorImg } = {}, articleSlug: { slug = '' } = {}, summary, categories } = blog

  return (
    <motion.div className='break-inside-avoid h-full w-full relative' initial='initial' animate='initial' whileHover='hover'>
      <Link href={slug} className='group'>
        {/* CARD IMAGE */}
        <Image
          placeholder='blur'
          blurDataURL={`data:image/svg+xml;base64,${toBase64(shimmer(700, 475))}`}
          alt={title}
          src={urlFor(thumbnailImg).url()}
          width='0'
          height='0'
          sizes='100vw'
          className='w-full h-auto object-cover rounded-3xl shadow-xl'
        />

        {/* CARD INFO */}
        <div className='flex-between pt-4 dark:text-darkGrey-tint-7 text-darkGrey-tint-2'>
          <div className='flex-center space-x-1'>
            <Image src={urlFor(authorImg).url()} alt={authorName} width={24} height={24} className='rounded-full h-6 w-6 mr-1' />
            <p className={['font-semibold text-sm'].filter(Boolean).join(' ')}>{authorName}</p>
          </div>
          <div className='flex-center space-x-1'>
            <CalendarDaysIcon className='h-5 w-5 ' />
            <p className={['font-semibold text-sm'].filter(Boolean).join(' ')}>{formatDate(publishedAt)}</p>
          </div>
        </div>
        <motion.div className='flex-center justify-start w-fit cursor-pointer gap-2 pt-4'>
          <h5 className='dark:text-darkGrey-tint-8 text-darkGrey'>{title}</h5>

          <motion.span variants={blogVariants}>
            <ArrowLongRightIcon className='w-6 h-auto dark:text-darkGrey-tint-8 text-darkGrey' />
          </motion.span>
        </motion.div>
        <p className={['pt-2 text-darkGrey-tint-5  font-light'].filter(Boolean).join(' ')}>{summary}</p>

        <div className='flex items-center space-x-4 mt-6'>
          {categories?.map(({ title, _id }) => (
            <Tag style={randomColor()} title={title} key={_id} className={['!border-0 !px-4'].filter(Boolean).join(' ')} />
          ))}
        </div>
      </Link>
    </motion.div>
  )
}

export default memo(BlogCard)
