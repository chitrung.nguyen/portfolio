import React, { memo } from 'react'
import Image from 'next/image'
import dynamic from 'next/dynamic'
import { motion } from 'framer-motion'
import { ArrowLongRightIcon } from '@heroicons/react/24/solid'

import { IBlog } from '@/types'
import { urlFor } from '@utils/sanity/sanity'
import { formatDate, randomColor } from '@/utils'
import { blogVariants } from '@utils/famer-motion'

const Tag = dynamic(() => import('@components/@cores').then(module => module.Tag), { ssr: false })

type Props = {
  blog: IBlog
}

const MostViewedBlog: React.FC<Props> = ({ blog }) => {
  return (
    <motion.div className='bg-white-primary dark:bg-darkGrey-primary w-full h-full min-h-[480px]'>
      <motion.div className='container-xl cursor-pointer' initial='initial' animate='initial' whileHover='hover'>
        <div className='relative min-h-[290px]'>
          <Image alt={blog.title} src={urlFor(blog.thumbnailImg).url()} fill className='object-cover rounded-3xl !-top-32 min-h-[418px] shadow-xl' />
        </div>
        <div className='mt-8 text-darkGrey'>
          <div className='dark:text-darkGrey-tint-7 text-darkGrey-tint-2'>
            <div className='flex items-center space-x-2'>
              <Image src={urlFor(blog.author.image).url()} alt={blog.author.name} width={32} height={32} className='rounded-full h-8 w-8' />
              <p className='font-semibold text-sm'>{blog.author.name}</p>
              <span className='font-semibold text-lg'>•</span>
              <p className={['font-semibold text-sm'].filter(Boolean).join(' ')}>{formatDate(blog.publishedAt)}</p>
            </div>
          </div>

          <div className='pt-2 flex items-center space-x-4'>
            <h4 className='dark:text-darkGrey-tint-8 text-darkGrey'>{blog.title}</h4>
            <motion.span variants={blogVariants}>
              <ArrowLongRightIcon className='w-10 h-auto dark:text-darkGrey-tint-8 text-darkGrey' />
            </motion.span>
          </div>
          <p className='pt-2 text-darkGrey-tint-5 font-normal'>{blog.summary}</p>
          <div className='flex items-center space-x-4 mt-6'>
            {blog.categories.map(category => (
              <Tag style={randomColor()} title={category.title} key={category._id} className={['!border-0 !px-4'].filter(Boolean).join(' ')} />
            ))}
          </div>
        </div>
      </motion.div>
    </motion.div>
  )
}

export default memo(MostViewedBlog)
