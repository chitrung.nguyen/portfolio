import React from 'react'
import { SEO } from '@/components'

type Props = {
  className?: string
}

const Loading: React.FC<Props> = ({ className }) => {
  return (
    <>
      <SEO />
      <div className={[className, 'loading'].filter(Boolean).join(' ')} />
    </>
  )
}

export default Loading
