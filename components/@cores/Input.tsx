import React, { ReactNode, useState } from 'react'

interface IInput {
  // REQUIRED
  value?: string | number | readonly string[] | undefined
  // OPTIONAL
  disabled?: boolean
  type?: string
  name?: string
  label?: string
  className?: string
  labelStyle?: string
  placeholder?: string
  classNameInput?: string
  addonIconBefore?: string | ReactNode
  addonIconAfter?: string | ReactNode
  variant?: 'standard' | 'outlined'
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
  onFocus?: () => void
  onBlur?: () => void
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  useForm?: any
}

const Input: React.FC<IInput> = ({
  className,
  classNameInput,
  label: labelText,
  labelStyle,
  value,
  placeholder,
  addonIconBefore,
  addonIconAfter,
  type = 'text',
  variant = 'outlined',
  disabled = false,
  name = '',
  onChange = () => undefined,
  onBlur = () => undefined,
  onFocus = () => undefined,
  useForm
}) => {
  const [focus, setFocus] = useState<boolean>(false)

  const onFocusInput = () => {
    onFocus()
    setFocus(true)
  }

  const onBlurInput = () => {
    onBlur()
    setFocus(false)
  }

  const outlinedInput = () => (
    <>
      {variant === 'outlined' && (
        <div className='inputOutlined'>
          {addonIconBefore && <div className={['inputOutlined__addonIconBefore', 'cursor-pointer'].join(' ')}>{addonIconBefore}</div>}
          <input
            {...useForm} // react-hook-form props
            {...(!useForm && { onChange })} // if react-hook-form props IS NOT USED, the "onChange" function will be applied
            name={name}
            value={value}
            disabled={disabled}
            type={type}
            className={[classNameInput, 'inputOutlined__input', addonIconBefore ? 'pl-[60px] pr-6' : 'pl-6 pr-14'].join(' ')}
            placeholder={placeholder}
          />
          {addonIconAfter && <div className={['inputOutlined__addonIconAfter', 'pl-2 cursor-pointer'].join(' ')}>{addonIconAfter}</div>}
        </div>
      )}
    </>
  )

  const standardInput = () => (
    <>
      {variant === 'standard' && (
        <div className={['inputStandard', focus ? 'inputStandard_focused' : ''].join(' ')}>
          {addonIconBefore && <div className={['inputStandard__addonIconBefore', 'cursor-pointer'].join(' ')}>{addonIconBefore}</div>}
          <input
            value={value}
            {...useForm} // react-hook-form props
            {...(!useForm && { onChange })} // if react-hook-form props IS NOT USED, the "onChange" function will be applied
            onFocus={onFocusInput}
            onBlur={onBlurInput}
            name={name}
            disabled={disabled}
            type={type}
            className={['inputStandard__input', addonIconBefore ? 'pl-8 pr-5' : 'pr-10', classNameInput].join(' ')}
            placeholder={placeholder}
          />
          {addonIconAfter && <div className={['inputStandard__addonIconAfter', 'pl-2 cursor-pointer'].join(' ')}>{addonIconAfter}</div>}
        </div>
      )}
    </>
  )

  return (
    <div className={['input-wrapper', className].join(' ')}>
      {/* LABEL TEXT */}
      {labelText && <label className={['input-wrapper__labelText mb-2', labelStyle].join(' ')}>{labelText}</label>}

      {/* INPUT FORM */}
      {outlinedInput()}
      {standardInput()}
    </div>
  )
}

export default Input
