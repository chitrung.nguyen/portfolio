import React, { memo } from 'react'

interface Props {
  title: string
  activeTag?: boolean
  activeTagColor?: string
  className?: string
  onClick?: () => void
  style?: any
}

const Tag: React.FC<Props> = ({ title = '', activeTag = false, onClick, className, style, activeTagColor }) => {
  return (
    <div
      onClick={onClick}
      style={{
        ...style
      }}
      className={['custom-tag', className, activeTag && activeTagColor].filter(Boolean).join(' ')}
    >
      {title}
    </div>
  )
}

export default memo(Tag)
