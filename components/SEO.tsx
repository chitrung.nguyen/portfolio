import React from 'react'
import Head from 'next/head'
import { useTranslation } from '@/hooks'

interface Props {
  isTranslate?: boolean
  title?: string
  description?: string
}

const SEO: React.FC<Props> = ({ title, description, isTranslate = true }) => {
  const t = useTranslation()

  const newTitle = isTranslate ? t(title ? title : '') : title
  const newDescription = isTranslate ? t(description ? description : '') : description

  const metaTitle = title ? `Portfolio | ${newTitle}` : 'Portfolio'
  let metaDescription = description ? newDescription : t('common.footer.bio')
  metaDescription = metaDescription.replace(/(\r\n|\n|\r)/gm, '')

  return (
    <>
      <Head>
        <title>{metaTitle}</title>
        <meta property='og:title' content={metaTitle} key='title' />
        <meta name='description' content={metaDescription} />
      </Head>
    </>
  )
}

export default SEO
