import React, { useContext, useRef, useState } from 'react'
import { motion } from 'framer-motion'
import { SubmitHandler, useForm } from 'react-hook-form'
import { ArrowLongRightIcon } from '@heroicons/react/24/solid'

import { SendEmail } from '@/utils'
import { useTranslation } from '@/hooks'
import { ThemeContext } from '@/context/ThemeContext'

import MailSent from '@public/icons/mail-sent.svg'
import MailSentSecondary from '@public/icons/mail-sent-secondary.svg'

import { Button, Input, Modal } from '@components/@cores'

type ContactFormType = {
  name: string
  email: string
  message: string
}

const Contact = () => {
  const [loading, setLoading] = useState<boolean>(false)
  const [hover, setHover] = useState<boolean>(false)
  const [open, setOpen] = useState<boolean>(false)
  const form = useRef<HTMLFormElement>(null)
  const [theme] = useContext(ThemeContext)
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset
  } = useForm<ContactFormType>()
  const t = useTranslation()

  const errorMsg: { [key: string]: string } = {
    name: t('contact.required_name'),
    email: t('contact.required_email'),
    message: t('contact.required_message')
  }

  const onSubmit: SubmitHandler<ContactFormType> = async data => {
    setLoading(true)
    if (data && data.name && data.message && data.email) {
      await SendEmail(form.current as HTMLFormElement).then(
        () => {
          setLoading(false)
          setOpen(true)
        },
        error => {
          console.log('error sending email: ', error.text)
        }
      )
    }
  }

  const errorMessage = (type: string, name: string) => {
    if (type !== 'required') return null
    return <span className='text-red-500 font-bold absolute -bottom-8'>{errorMsg[name]}</span>
  }

  return (
    <div className={['h-full min-h-auto py-10 tablet:py-20 bg-contact-img'].filter(Boolean).join(' ')}>
      <div className='text-center text-white'>
        <h3 className='capitalize'>
          <span className='font-thin capitalize'>{t('contact.title')} </span>
          {t('contact.sub_title')}
        </h3>
        <p>{t('contact.sub_title_1')}</p>
      </div>
      <form ref={form} className='container-xl max-w-[960px] mt-10' onSubmit={handleSubmit(onSubmit)}>
        <div className='grid grid-cols-1 md:grid-cols-2 gap-6 md:gap-10'>
          <div className='relative'>
            <Input
              useForm={{
                ...register('name', {
                  required: true
                })
              }}
              name='name'
              variant='standard'
              label={t('contact.label_name')}
              placeholder={t('contact.placeholder_name')}
              labelStyle={[errors.name ? 'text-red-500' : 'text-white'].filter(Boolean).join(' ')}
              className={[errors.name ? 'custom-input-wrapper-error' : 'custom-input-wrapper', 'w-full min-w-full'].filter(Boolean).join(' ')}
              classNameInput={[errors.name ? 'custom-input-error !border-red-500' : 'custom-input !border-darkGrey-tint-8', '!text-white !bg-transparent border border-solid']
                .filter(Boolean)
                .join(' ')}
            />
            {errorMessage(errors?.name?.type as string, 'name')}
          </div>
          <div className='relative'>
            <Input
              useForm={{
                ...register('email', {
                  required: true
                })
              }}
              name='email'
              variant='standard'
              label={t('contact.label_email')}
              placeholder={t('contact.placeholder_email')}
              labelStyle={[errors.email ? 'text-red-500' : 'text-white'].filter(Boolean).join(' ')}
              className={[errors.email ? 'custom-input-wrapper-error' : 'custom-input-wrapper', 'w-full min-w-full'].filter(Boolean).join(' ')}
              classNameInput={[errors.email ? 'custom-input-error !border-red-500' : 'custom-input !border-darkGrey-tint-8', '!text-white !bg-transparent border border-solid']
                .filter(Boolean)
                .join(' ')}
            />
            {errorMessage(errors?.email?.type as string, 'email')}
          </div>
        </div>
        <div className='flex flex-col justify-center relative mt-6 md:mt-10'>
          <label className={['text-base leading-[22px] font-semibold transition-all duration-300 mb-2', errors.message ? 'text-red-500' : 'text-white'].filter(Boolean).join(' ')}>
            {t('contact.label_message')}
          </label>
          <textarea
            {...register('message', {
              required: true
            })}
            name='message'
            placeholder={t('contact.placeholder_message')}
            className={[
              errors.message
                ? 'focus:ring-red-500 border-red-500 placeholder:text-red-500'
                : 'focus:ring-white border-darkGrey-tint-8 tablet:placeholder:text-darkGrey-tint-6 tablet_max:placeholder:text-sm',
              'bg-transparent border border-solid rounded-lg min-h-[164px] p-5 transition-all duration-300 focus:outline-0 focus:outline-none focus:ring-2 text-base text-white font-normal leading-[140%] tracking-[0.44px]'
            ]
              .filter(Boolean)
              .join(' ')}
          />
          {errorMessage(errors?.message?.type as string, 'message')}
        </div>
        <div className='mt-6 md:mt-10 md:flex items-center justify-end'>
          <motion.div
            onMouseEnter={() => {
              setHover(true)
            }}
            onMouseLeave={() => {
              setHover(false)
            }}
            whileTap={{ scale: 0.9 }}
            whileHover={{ scale: 1.1 }}
            transition={{ type: 'spring', stiffness: 400, damping: 8, duration: 0.4 }}
            className='group'
          >
            <Button
              loading={loading}
              loadingColor={hover ? 'white' : 'darkGrey'}
              suffix={loading ? null : <ArrowLongRightIcon className='text-darkGrey w-full h-5 group-hover:text-white transition-all duration-300' />}
              type='submit'
              className='w-full h-12 border-darkGrey-shade-1 text-darkGrey min-w-[200px] capitalize font-bold bg-white dark:group-hover:bg-primary group-hover:bg-secondary group-hover:text-white dark:group-hover:border-primary group-hover:border-secondary transition-all duration-300'
            >
              {t('contact.send_message')}
            </Button>
          </motion.div>
        </div>
      </form>
      <Modal
        open={open}
        onClose={() => {
          reset()
          setOpen(false)
        }}
      >
        <div>
          {theme === 'dark' ? <MailSent className='w-40 h-40 mx-auto' /> : <MailSentSecondary className='w-40 h-40 mx-auto' />}
          <h6 className='text-darkGrey text-center mt-8'>You submitted successfully !</h6>
        </div>
      </Modal>
    </div>
  )
}

export default Contact
