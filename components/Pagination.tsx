import React from 'react'
import Link from 'next/link'
import { ArrowLongLeftIcon, ArrowLongRightIcon } from '@heroicons/react/24/solid'

interface Props {
  onNext?: () => void
  onPrev?: () => void
  onPageChange?: (page: number) => void
  items: number
  currentPage?: number
  pageSize?: number
}

const Pagination: React.FC<Props> = ({ onNext = () => undefined, onPrev, onPageChange = () => undefined, items, currentPage = 1, pageSize = 2 }) => {
  /* 
  Example:
    items: 100 (data)
    currentPage: 1
    pageSize: 10
  */

  const pagesCount = Math.ceil(items / pageSize) // 100/10
  if (pagesCount === 1) return null

  const pages = Array.from({ length: pagesCount }, (_, i) => i + 1) // [1,2,3,4,...,10]

  return (
    <div className='flex-between w-full h-auto space-x-4'>
      <Link href={`#page=${currentPage - 1}`} shallow onClick={onPrev} className='cursor-pointer text-darkGrey-tint-5 flex-center space-x-2'>
        <ArrowLongLeftIcon className='w-6 h-6' />
        <p>Previous page</p>
      </Link>

      <ul className='flex-center space-x-4'>
        {pages.map(page => (
          <li key={page}>
            <Link
              href={`#page=${page}`}
              shallow
              onClick={() => onPageChange(page)}
              className={[page === currentPage ? 'dark:bg-primary bg-secondary text-white-primary' : 'dark:text-darkGrey-tint-5 text-darkGrey', 'w-10 h-10 flex-center rounded-lg']
                .filter(Boolean)
                .join(' ')}
            >
              {page}
            </Link>
          </li>
        ))}
      </ul>

      <Link href={`#page=${currentPage + 1}`} shallow onClick={onNext} className='cursor-pointer text-darkGrey-tint-5 flex-center space-x-2'>
        <p>Next page</p>
        <ArrowLongRightIcon className='w-6 h-6' />
      </Link>
    </div>
  )
}

export default Pagination
