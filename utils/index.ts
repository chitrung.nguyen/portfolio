import emailjs from '@emailjs/browser'

import { currentLanguage, defaultLocale } from '@/i18n'

export const hrefUrl = (url: string) => {
  const language = currentLanguage()
  if (defaultLocale !== language) {
    return `/${language}${url}`
  }

  return url
}

export const formatPathname = (pathname: string) => {
  const language = currentLanguage()
  return language === defaultLocale ? pathname : (pathname as string).replace(`/${language}`, '')
}

export const formatDate = (date: string) => {
  return new Date(date).toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric'
  })
}

export const SendEmail = async (formData: HTMLFormElement) => {
  const serviceID = process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID || ''
  const templateID = process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_ID || ''
  const publicKey = process.env.NEXT_PUBLIC_EMAILJS_PUBLIC_KEY || ''

  return await emailjs.sendForm(serviceID, templateID, formData, publicKey)
}

export const shimmer = (w: number, h: number) => `
  <svg width="${w}" height="${h}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
      <linearGradient id="g">
        <stop stop-color="#818080" offset="20%" />
        <stop stop-color="#29323C" offset="50%" />
        <stop stop-color="#818080" offset="70%" />
      </linearGradient>
    </defs>
    <rect width="${w}" height="${h}" fill="#818080" />
    <rect id="r" width="${w}" height="${h}" fill="url(#g)" />
    <animate xlink:href="#r" attributeName="x" from="-${w}" to="${w}" dur="1s" repeatCount="indefinite"  />
  </svg>`

export const toBase64 = (str: string) => (typeof window === 'undefined' ? Buffer.from(str).toString('base64') : window.btoa(str))

export const randomColor = () => {
  const random = Math.floor(Math.random() * 8)

  switch (random) {
    case 1:
      // purple (Secondary)
      return {
        backgroundColor: '#F9F5FF',
        color: '#805ad5'
      }
    case 2:
      // blue
      return {
        backgroundColor: '#EEF4FF',
        color: '#0EA5E9'
      }
    case 3:
      // pink
      return {
        backgroundColor: '#FDF2FA',
        color: '#C11574'
      }
    case 4:
      // green
      return {
        backgroundColor: '#ECFDF3',
        color: '#027A48'
      }
    case 5:
      // orange
      return {
        backgroundColor: '#FFF4ED',
        color: '#B93815'
      }
    case 6:
      // blue light
      return {
        backgroundColor: '#F0F9FF',
        color: '#026AA2'
      }
    case 7:
      // rose
      return {
        backgroundColor: '#FFF1F3',
        color: '#C01048'
      }

    default:
      return {
        backgroundColor: '#F8F9FC',
        color: '#363F72'
      }
  }
}
