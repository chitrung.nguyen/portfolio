export const getBlogs = `*[_type == 'articleBlogs' && language == $lang] | order(dateTime(publishedAt) desc) [$from...$to] {
    _id,
    title,
    author -> {
      _id,
      name,
      image
    },
    summary,
    thumbnailImg,
    publishedAt,
    'articleSlug':  articleSlug[0] -> { _id, 'slug': slug.current },
    'categories':  categories[] -> { _id, title },
    'pageNumber': count(*[_type == "articleBlogs" && language == $lang]),
  }`

export const getCategories = `*[_type == 'category'] {
  _id,
  title
}`

export const filterBlogsByCategories = ({ lang, activeCategory }: { lang: string; activeCategory: string }) => {
  return `
    *[_type == 'articleBlogs' && language == '${lang}' && '${activeCategory}' in categories[]{_type == 'reference' => @-> { _id }}._id ] {
      _id,
      title,
      author -> {
        _id,
        name,
        image
      },
      summary,
      thumbnailImg,
      publishedAt,
      'articleSlug':  articleSlug[0] -> { _id, 'slug': slug.current },
      'categories':  category[0] -> { _id, title },
    }
  `
}
