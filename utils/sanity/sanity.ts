/* eslint-disable @typescript-eslint/no-explicit-any */
import { createClient } from 'next-sanity'
import createImageUrlBuilder from '@sanity/image-url'

type IConfig = {
  dataset: string
  projectId: string
  apiVersion: string
  useCdn: boolean
}

const projectId = process.env.NEXT_PUBLIC_SANITY_PROJECT_ID || ''
const dataset = process.env.NEXT_PUBLIC_SANITY_DATASET || 'production'
const apiVersion = '2022-02-02'

export const config: IConfig = {
  dataset,
  projectId,
  apiVersion,
  useCdn: process.env.NODE_ENV === 'production'
}

export const sanityClient = createClient(config)

export const urlFor = (source: any) => createImageUrlBuilder(config).image(source)
