import { Variants } from 'framer-motion'

export const blogVariants: Variants = {
  initial: {
    opacity: 0,
    scaleX: 0,
    transformOrigin: 'left'
  },
  hover: {
    opacity: 1,
    scaleX: 1,
    color: '',
    transformOrigin: 'left'
  }
}
