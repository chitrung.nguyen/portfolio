import { useEffect, useState } from 'react'
import type { AppProps } from 'next/app'
import NextNProgress from 'nextjs-progressbar'

import { currentLanguage } from '@/i18n'
import RootLayout from '@/layouts'
import { ThemeProvider as ThemeProviderContext } from '@context/ThemeContext'
import { LoadingProvider as LoadingProviderContext } from '@context/LoadingContext'

import '@styles/globals.scss'

function MyApp({ Component, pageProps }: AppProps) {
  const [show, setShow] = useState<boolean>(false)
  const language = currentLanguage()

  useEffect(() => {
    updateHtmlLang()
  }, [language])

  useEffect(() => {
    document.fonts.ready.then(() => {
      setShow(true)
    })
  }, [])

  const updateHtmlLang = () => {
    const selectorHTML = document.querySelector('html') as HTMLHtmlElement
    selectorHTML.lang = language as string
  }

  return (
    <ThemeProviderContext>
      <LoadingProviderContext initialLoading={!show}>
        <RootLayout>
          <NextNProgress color='#F87171' startPosition={0.3} stopDelayMs={200} height={4} showOnShallow={true} options={{ easing: 'ease-in-out', speed: 500 }} />
          <Component {...pageProps} />
        </RootLayout>
      </LoadingProviderContext>
    </ThemeProviderContext>
  )
}

export default MyApp
