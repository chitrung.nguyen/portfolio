import Document, { Head, Html, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang='en'>
        <meta content='width=device-width, initial-scale=1' name='viewport' />
        <Head>
          <link rel='icon' type='image/png' href='/favicon.ico' />
          <link rel='preconnect' href='https://fonts.googleapis.com' />
          <link rel='preconnect' href='https://fonts.gstatic.com' crossOrigin='' />
          <link rel='preconnect' href='https://fonts.googleapis.com' />
          <link href='https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap' rel='stylesheet' />
        </Head>
        <body>
          <div id='portal' />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
