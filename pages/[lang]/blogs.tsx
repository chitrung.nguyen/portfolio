import { useEffect, useLayoutEffect, useRef, useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { GetServerSideProps, NextPage } from 'next'
import { AnimatePresence, motion } from 'framer-motion'
import { AdjustmentsVerticalIcon } from '@heroicons/react/24/solid'

import { IBlog } from '@/types'
import { currentLanguage } from '@/i18n'
import { sanityClient } from '@utils/sanity/sanity'
import { filterBlogsByCategories, getBlogs, getCategories } from '@utils/sanity/blogs-query'

import { useTranslation } from '@/hooks'

import { Loading } from '@components/@cores'
import { BlogCard, MostViewedBlog, Pagination, SEO } from '@/components'
const Tag = dynamic(() => import('@components/@cores').then(module => module.Tag), { ssr: false })

interface Props {
  blogs: IBlog[]
  categories: { _id: string; title: string }[]
}

const pageSize = 6

const BlogsPage: NextPage<Props> = ({ blogs, categories }) => {
  const [loading, setLoading] = useState<boolean>(false)
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [blogsData, setBlogsData] = useState<IBlog[]>(blogs)
  const [activeCategory, setActiveCategory] = useState<string>('')
  const [fromTo, setFromTo] = useState<{ from: number; to: number }>({ from: 0, to: pageSize })

  const router = useRouter()
  const ref = useRef<HTMLDivElement>(null)

  const firstBlog = blogs[0]
  const lang = currentLanguage()
  const t = useTranslation()

  useLayoutEffect(() => {
    if (router.asPath.includes('#page=')) {
      const hashNumber = router.asPath.split('#page=')[1]
      onPageChange(+hashNumber)
    }
  }, [])

  useEffect(() => {
    if (activeCategory) {
      filterBlogsByCategory()
    }
  }, [activeCategory])

  const getBlogsHandler = async (fromToClone: { from: number; to: number }) => {
    setLoading(true)
    const data = await sanityClient.fetch(getBlogs, { lang, ...fromToClone })

    if (data.length > 0) {
      setBlogsData([...data])
      setFromTo({ ...fromToClone })
    }
    setLoading(false)
  }

  const filterBlogsByCategory = async () => {
    const query = filterBlogsByCategories({ lang, activeCategory })

    if (activeCategory !== 'all') {
      setLoading(true)
      const data = await sanityClient.fetch(query)
      setBlogsData([...data])
      setLoading(false)
    } else {
      getBlogsHandler(fromTo)
    }
  }

  const fetchBlogsPerPage = async (type: string) => {
    ref.current?.scrollIntoView({ behavior: 'smooth' })
    setTimeout(() => {
      const page = type === 'prev' ? currentPage - 1 : currentPage + 1
      const fromToClone = type === 'prev' ? { from: fromTo.from - pageSize, to: fromTo.to - pageSize } : { from: fromTo.from + pageSize, to: fromTo.to + pageSize }

      getBlogsHandler(fromToClone)
      setCurrentPage(page)
    }, 500)
  }

  const onPageChange = (page: number) => {
    ref.current?.scrollIntoView({ behavior: 'smooth' })
    setTimeout(() => {
      setCurrentPage(page)

      const numStart = pageSize * (page - 1)
      const fromToClone = { from: numStart, to: numStart + pageSize }
      getBlogsHandler(fromToClone)
    }, 500)
  }

  return (
    <div className='w-full h-auto pt-20 dark:bg-white-primary bg-darkGrey-primary'>
      <SEO title='blogs_page.meta_title' description='blogs_page.meta_description' />

      <div className='h-[382px] relative'>
        <div className='text-center mx-auto mb-20 max-w-[768px]'>
          <p className='font-semibold text-white-primary dark:text-darkGrey'>{t('blogs_page.meta_title')}</p>
          <h3 className='mt-3 font-semibold text-white dark:text-darkGrey -tracking-[0.02em]'>{t('blogs_page.meta_subTitle')}</h3>
          <h6 className='mt-6 font-normal text-white-primary dark:text-darkGrey'>{t('blogs_page.meta_description')}</h6>
        </div>
      </div>

      <MostViewedBlog blog={firstBlog} />

      <div ref={ref} className='pt-16 pb-20 bg-white-primary dark:bg-darkGrey-primary container-xxl'>
        <div className='container-xl text-darkGrey'>
          <div className='flex items-center space-x-2'>
            <AdjustmentsVerticalIcon className='w-5 h-auto text-secondary-blue' />
            <p className='font-normal text-secondary-blue'>Blog categories</p>
          </div>

          <div className='mt-2 flex items-center flex-wrap space-x-2'>
            <Tag
              className='!py-2 px-4 bg-transparent border-0 text-darkGrey-tint-5'
              key='all'
              title='All'
              activeTag={activeCategory === 'all' || activeCategory === ''}
              activeTagColor='!bg-[#EEF4FF]'
              onClick={() => setActiveCategory(activeCategory && activeCategory === 'all' ? '' : 'all')}
            />
            {categories.map(category => (
              <Tag
                className='py-2 px-4 bg-transparent border-0 text-darkGrey-tint-5'
                key={category._id}
                title={category.title}
                activeTag={activeCategory === category._id}
                activeTagColor='!bg-[#EEF4FF]'
                onClick={() => setActiveCategory(activeCategory && activeCategory === category._id ? '' : category._id)}
              />
            ))}
          </div>

          <hr className='my-4 dark:border-t-darkGrey-tint-4 border-t-darkGrey-tint-8' />

          <motion.div initial={{ y: 10, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: -10, opacity: 0 }} transition={{ duration: 0.75 }} className='mt-8 gap-8 columns-2 space-y-12'>
            {blogsData.map(blog => (
              <BlogCard blog={blog} key={blog._id} />
            ))}
          </motion.div>

          <hr className='mt-16 mb-8 dark:border-t-darkGrey-tint-4 border-t-darkGrey-tint-8' />

          <Pagination
            items={blogs[0].pageNumber}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={onPageChange}
            onNext={() => fetchBlogsPerPage('next')}
            onPrev={() => fetchBlogsPerPage('prev')}
          />
        </div>
      </div>

      <AnimatePresence mode='wait'>
        {loading && (
          <motion.div
            initial={{ opacity: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            animate={{ opacity: 1, transformOrigin: 'top', pointerEvents: 'auto' }}
            exit={{ opacity: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            className='fixed inset-0 bg-loadingWrapper-light dark:bg-loadingWrapper-dark z-[9999] flex-center'
          >
            <Loading />
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  )
}

export default BlogsPage

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const blogs = await sanityClient.fetch(getBlogs, { lang: params?.lang, from: 0, to: 6 })
  const categories = await sanityClient.fetch(getCategories)

  return {
    props: {
      blogs,
      categories
    }
  }
}
