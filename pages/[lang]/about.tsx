import { useTranslation } from '@/hooks'
import { getLocalePartsFrom, locales } from '@/i18n'

export async function generateStaticParams() {
  return locales.map(locale => getLocalePartsFrom({ locale }))
}

const AboutPage = ({ params }: any) => {
  const t = useTranslation()
  return (
    <div>
      <p className='text-red-400'>About page</p>
      <h1>{t('welcome.helloWorld')}</h1>
      <h2>
        {t('welcome.happyYear', {
          year: new Date().getFullYear()
        })}
      </h2>
    </div>
  )
}

export default AboutPage
