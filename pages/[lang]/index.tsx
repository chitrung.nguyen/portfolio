import type { NextPage } from 'next'

import { getLocalePartsFrom, locales } from '@/i18n'
import { useTranslation } from '@/hooks/useTranslation'
import { SEO } from '@/components'

export function generateStaticParams() {
  return locales.map(locale => getLocalePartsFrom({ locale }))
}

const Home: NextPage = () => {
  const t = useTranslation()

  return (
    <div>
      <SEO title='home.meta_title' description='common.author' />
      <p className='text-red-400'>Home page {t('welcome.helloWorld')}</p>
      {t('welcome.happyYear', {
        year: new Date().getFullYear()
      })}
    </div>
  )
}

export default Home
