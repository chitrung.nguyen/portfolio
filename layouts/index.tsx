import React, { useContext } from 'react'
import { usePathname } from 'next/navigation'
import { AnimatePresence, motion } from 'framer-motion'

import { LoadingContext } from '@context/LoadingContext'

import { Contact } from '@/components'
import { Loading } from '@components/@cores'
import Footer from './footer'
import Header from './header'

const RootLayout = ({ children }: React.PropsWithChildren) => {
  const pathname = usePathname()
  const [loading] = useContext(LoadingContext)

  if (loading) {
    return (
      <motion.div
        initial={{ opacity: 0, transformOrigin: 'top', pointerEvents: 'none' }}
        animate={{ opacity: 1, transformOrigin: 'top', pointerEvents: 'auto' }}
        exit={{ opacity: 0, transformOrigin: 'top', pointerEvents: 'none' }}
        className='fixed inset-0 bg-darkGrey-primary z-[9999] flex-center'
      >
        <Loading />
      </motion.div>
    )
  }

  return (
    <AnimatePresence mode='wait'>
      <div className='relative' id='default-layout'>
        <Header />
        <motion.main key={pathname} initial={{ y: 10, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: -10, opacity: 0 }} transition={{ duration: 0.75 }} className='w-full h-auto mt-[72px]'>
          {children}
        </motion.main>
        <Contact />
        <Footer />
      </div>
    </AnimatePresence>
  )
}

export default RootLayout
