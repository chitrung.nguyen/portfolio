import React from 'react'
import Link from 'next/link'

import FacebookIcon from '@public/icons/facebook.svg'
import GithubIcon from '@public/icons/github.svg'
import GitlabIcon from '@public/icons/gitlab.svg'
import GmailIcon from '@public/icons/gmail.svg'
import LinkedinIcon from '@public/icons/linkedin.svg'

import { useTranslation } from '@/hooks'

const Footer = () => {
  const t = useTranslation()

  const connectIcons = () => (
    <div className='flex items-center gap-2'>
      <Link href='https://www.linkedin.com/in/nguyen-chi-trung-5631761aa/' target='_blank'>
        <LinkedinIcon className='cursor-pointer' />
      </Link>
      <Link href='https://github.com/NguyenChiTrung1310' target='_blank'>
        <GithubIcon className='[&>path]:fill-white-primary cursor-pointer' />
      </Link>
      <Link href='https://gitlab.com/chitrung.nguyen' target='_blank'>
        <GitlabIcon className='cursor-pointer' />
      </Link>
      <Link href='https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=nguyenchitrung1310@gmail.com' target='_blank'>
        <GmailIcon className='cursor-pointer' />
      </Link>
      <Link href='https://www.facebook.com/chitrung.nguyen.5832' target='_blank'>
        <FacebookIcon className='cursor-pointer' />
      </Link>
    </div>
  )

  const desktopView = () => (
    <div className='text-white-primary'>
      <div className='grid grid-cols-4 gap-4 lg:gap-12'>
        <div className='flex flex-col justify-center col-span-2'>
          <h6 className='uppercase font-medium font-roboto'>{t('common.footer.about')}</h6>
        </div>
        <div className='flex flex-col justify-center col-span-1'>
          <h6 className='uppercase font-medium font-roboto'>{t('common.footer.connect')}</h6>
        </div>
        <div className='flex flex-col justify-center col-span-1'>
          <h6 className='uppercase font-medium font-roboto'>{t('common.footer.contact')}</h6>
        </div>
      </div>
      <div className='grid grid-cols-4 gap-4 lg:gap-12'>
        <div className='pt-5 col-span-2'>
          <p className='text-sm whitespace-pre-line'>{t('common.footer.bio')}</p>
        </div>
        <div className='pt-5 col-span-1'>{connectIcons()}</div>
        <div className='pt-5 col-span-1'>
          <p className='text-sm'>+84 963742905</p>
          <p className='pt-2 text-sm'>nguyenchitrung1310@gmail.com</p>
        </div>
      </div>
      <div className='text-center mt-10'>
        <hr />
        <p className='mt-10 text-sm'>
          Copyright ©2022 All rights reserved | This portfolio is made by <span className='font-bold font-roboto text-primary'>Trung Nguyen</span>
        </p>
      </div>
    </div>
  )

  const mobileView = () => (
    <div className='text-white-primary'>
      <div className=''>
        <h6 className='uppercase font-roboto'>{t('common.footer.about')}</h6>
      </div>
      <div className='pt-5'>
        <p className='text-sm whitespace-pre-line'>{t('common.footer.bio')}</p>
      </div>
      <div className='mt-10 flex xs_max:flex-col items-start justify-between gap-10 xs:gap-16'>
        <div className='xs:w-1/2'>
          <h6 className='uppercase font-roboto'>{t('common.footer.connect')}</h6>
          <div className='pt-5'>{connectIcons()}</div>
        </div>
        <div className='xs:w-1/2'>
          <h6 className='uppercase font-roboto'>{t('common.footer.contact')}</h6>
          <p className='text-sm pt-5'>+84 963742905</p>
          <p className='pt-2 text-sm'>nguyenchitrung1310@gmail.com</p>
        </div>
      </div>
      <div className='text-center pt-10'>
        <hr />
        <p className='hidden xs:block mt-10 text-sm'>
          Copyright ©2022 All rights reserved | This portfolio is made by <span className='font-bold text-primary'>Trung Nguyen</span>
        </p>
        <p className='block xs:hidden mt-10 text-sm'>Copyright ©2022 All rights reserved</p>
        <p className='block xs:hidden text-sm'>
          This portfolio is made by <span className='font-bold font-roboto text-primary'>Trung Nguyen</span>
        </p>
      </div>
    </div>
  )

  return (
    <footer className='h-auto footer-gradient'>
      <div className='container-xl h-auto tablet:flex tablet:items-center py-10'>
        <div className='hidden tablet:block'>{desktopView()}</div>
        <div className='block tablet:hidden'>{mobileView()}</div>
      </div>
    </footer>
  )
}

export default Footer
