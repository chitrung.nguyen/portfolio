import React, { memo, useContext } from 'react'
import Link from 'next/link'
import { AnimatePresence, motion } from 'framer-motion'

import { hrefUrl } from '@/utils'
import { ROUTE } from '@utils/routes'
import { useScrollView } from '@/hooks'
import { ThemeContext } from '@context/ThemeContext'

import LogoDark from '@public/icons/logo-dark.svg'
import LogoLight from '@public/icons/logo-light.svg'

import { DarkmodeSwitcher, LanguageSwitcher, Menu, MenuMobile } from '@/components'

const Header = () => {
  const [theme] = useContext(ThemeContext)
  const { scrollY, visible } = useScrollView()

  return (
    <header>
      <AnimatePresence mode='wait'>
        {visible && (
          <motion.nav
            initial={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            animate={{ scaleY: 1, transformOrigin: 'top', pointerEvents: 'auto' }}
            exit={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            className={['w-full h-[72px] bg-white-shade-1 dark:layout-gradient fixed top-0 left-0 z-[1000] transition-all duration-75', scrollY > 10 && 'shadow-lg'].filter(Boolean).join(' ')}
          >
            <div className='h-full flex-between container-xl'>
              <Link className='text-darkGrey dark:text-white-primary flex-center' href={hrefUrl(ROUTE.HOME)}>
                {theme === 'dark' ? <LogoDark /> : <LogoLight />}
              </Link>

              {/* MENU LIST */}
              <div className='hidden tablet:block'>
                <Menu />
              </div>

              {/* ACTIONS MENU */}
              <div className='flex-center space-x-2'>
                <div className='hidden tablet:block'>
                  <LanguageSwitcher />
                </div>
                <DarkmodeSwitcher />
                <div className='block tablet:hidden'>
                  <MenuMobile />
                </div>
              </div>
            </div>
          </motion.nav>
        )}
      </AnimatePresence>
    </header>
  )
}

export default memo(Header)
