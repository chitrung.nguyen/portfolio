import { usePathname } from 'next/navigation'
import React, { createContext, useEffect, useState } from 'react'

interface ILoading {
  loading?: boolean
  initialLoading?: boolean
}

type LoadingType = ILoading | boolean

type LoadingContext = [LoadingType, React.Dispatch<React.SetStateAction<LoadingType>>]

export const LoadingContext = createContext<LoadingContext>([false, () => null])

export const LoadingProvider = ({ initialLoading, children }: React.PropsWithChildren<ILoading>) => {
  const [loading, setLoading] = useState<LoadingType>(false)
  const pathname = usePathname()

  useEffect(() => {
    if (pathname) {
      setLoading(false)
    } else {
      setLoading(true)
    }
  }, [pathname])

  useEffect(() => {
    setLoading(initialLoading as boolean)
  }, [initialLoading])

  return <LoadingContext.Provider value={[loading, setLoading]}>{children}</LoadingContext.Provider>
}
