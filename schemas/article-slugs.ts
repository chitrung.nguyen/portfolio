import { defineField, defineType } from 'sanity'

export default defineType({
  title: 'Article Slugs',
  name: 'articleSlugs',
  type: 'document',
  fields: [
    defineField({
      type: 'string',
      name: 'articleName',
      title: 'Article name'
    }),
    defineField({
      type: 'text',
      name: 'description',
      title: 'Description'
    }),
    defineField({
      type: 'slug',
      name: 'slug',
      title: 'Slug',
      options: {
        source: 'articleName',
        maxLength: 96
      }
    })
  ]
})
