import articleBlogs from './article-blogs'
import articleSlugs from './article-slugs'
import author from './author'
import blockContent from './blockContent'
import category from './category'
import localeString from './localeString'
import projectSlugs from './project-slugs'
import projectList from './projects-list'
import projectTypes from './projects-type'

export const schemaTypes: any = [localeString, articleSlugs, articleBlogs, author, category, blockContent, projectSlugs, projectTypes, projectList]
