import { defineType } from 'sanity'

type lang = {
  id: string
  title: string
  isDefault?: boolean
}

export const supportedLanguages: lang[] = [
  { id: 'en', title: 'English', isDefault: true },
  { id: 'vi', title: 'Vietnamese' }
]

export const baseLanguage = supportedLanguages.find(l => l.isDefault) as lang

const localeString = {
  type: 'object',
  name: 'localeString',
  title: 'Localized string',
  fieldsets: [
    {
      title: 'Translations',
      name: 'translations'
    }
  ],
  // Dynamically define one field per language
  fields: supportedLanguages.map(lang => ({
    type: 'string',
    name: lang.id,
    title: lang.title,
    fieldset: lang.isDefault ? null : 'translations'
  }))
}

export default defineType(localeString)
