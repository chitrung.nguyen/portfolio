import { defineField, defineType } from 'sanity'

const articles = {
  title: 'Article Blogs',
  name: 'articleBlogs',
  type: 'document',
  fields: [
    defineField({
      type: 'string',
      name: 'language',
      title: 'Select Language',
      options: {
        list: [
          { title: 'English', value: 'en' },
          { title: 'Vietnamese', value: 'vi' }
        ]
      }
    }),
    defineField({
      type: 'array',
      name: 'articleSlug',
      title: 'Article Slug',
      of: [{ type: 'reference', to: { type: 'articleSlugs' } }]
    }),
    defineField({
      type: 'string',
      name: 'title',
      title: 'Title'
    }),
    defineField({
      type: 'string',
      name: 'summary',
      title: 'Summary'
    }),
    defineField({
      type: 'reference',
      name: 'author',
      title: 'Author',
      to: { type: 'author' }
    }),
    defineField({
      type: 'image',
      name: 'thumbnailImg',
      title: 'Thumbnail image',
      options: {
        hotspot: true
      }
    }),
    defineField({
      type: 'image',
      name: 'mainImage',
      title: 'Main image',
      options: {
        hotspot: true
      }
    }),
    defineField({
      type: 'array',
      name: 'categories',
      title: 'Categories',
      of: [{ type: 'reference', to: { type: 'category' } }]
    }),
    defineField({
      type: 'datetime',
      name: 'publishedAt',
      title: 'Published at',
      options: {
        dateFormat: 'YYYY-MM-DD',
        timeFormat: 'HH:mm'
      }
    }),
    defineField({
      type: 'blockContent',
      name: 'body',
      title: 'Body'
    })
  ],
  preview: {
    select: {
      title: 'title',
      author: 'author.name',
      media: 'thumbnailImg'
    },
    prepare(selection: any) {
      const { author } = selection
      return { ...selection, subtitle: author && `by ${author}` }
    }
  }
}

export default defineType(articles)
