/* eslint-disable @typescript-eslint/no-explicit-any */

type BlogsBase = {
  _id: string
  _createdAt: string
  _updatedAt: string
}

export interface IBlog extends BlogsBase {
  title: string
  language: string
  articleSlug: {
    _id: string
    slug: string
  }
  categories: {
    _id: string
    title: string
  }[]
  thumbnailImg: {
    asset: any
  }
  mainImage: {
    asset: any
  }
  author: {
    _id: string
    name: string
    image: {
      asset: any
    }
  }
  summary: string
  publishedAt: string
  body: any
  pageNumber: number
}
