import { defineConfig } from 'sanity'
import { deskTool } from 'sanity/desk'
import { visionTool } from '@sanity/vision'
import { schemaTypes } from './schemas'

export default defineConfig({
  basePath: '/studio',
  name: 'Portfolio_Blog_Content',
  title: 'Portfolio Blog Content',

  projectId: process.env.NEXT_PUBLIC_SANITY_PROJECT_ID || '',
  dataset: process.env.NEXT_PUBLIC_SANITY_PROJECT_DATASET || '',

  plugins: [deskTool(), visionTool()],

  schema: {
    types: schemaTypes
  }
})
